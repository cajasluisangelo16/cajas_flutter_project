import 'package:flutter/material.dart';

class CartItem extends StatelessWidget {
  final String imagePath;
 
  const CartItem({
    super.key,
    required this.imagePath,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25.0),
      child: GestureDetector(
        onTap: (){
          
        },
        child: Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.purple, width: 2),
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset('lib/images/product.png',height: 100, width: 100,),
                const SizedBox(width: 40),
          
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    Text('Adidas Shoe    ',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14),),
                    SizedBox(height: 10),
                    Text('Qty: 1                ',
                    style: TextStyle(color: Colors.grey, fontSize: 14),),
                    SizedBox(height: 10),
                    Text('Php 11,999.00',
                    style: TextStyle(color: Colors.purple, fontSize: 14, fontStyle: FontStyle.italic),)
                  ],
                ),
                const SizedBox(width: 70)
              ],
            ),
          ),
          ),
      ),
    );
  }
}