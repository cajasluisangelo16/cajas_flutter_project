import 'package:flutter/material.dart';

class ItemContainer extends StatelessWidget {
  final String imagePath;
  const ItemContainer({
    super.key,
    required this.imagePath,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.purple, width: 2),
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('lib/images/product.png',height: 112, width: 112,),
            const SizedBox(height: 10),
            const Text('Adidas Shoe',style: TextStyle(fontWeight: FontWeight.bold),),
            const SizedBox(height: 5),
            const Text('Php 11,999.00'),
            const SizedBox(height: 10)
          ],
        ),
      ),
      );
  }
}