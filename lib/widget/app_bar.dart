import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget{

  
  const MyAppBar({
     Key? key,
    
  }) : super (key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(automaticallyImplyLeading: false,
      centerTitle: true,backgroundColor: Colors.white,
        title: Row( 
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            SizedBox(height:10),

            Text('SHOE', 
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32,color: Colors.black),),

            SizedBox(width: 8),

            Text('AVENUE', 
            style: TextStyle(fontWeight: FontWeight.bold,fontSize: 32,color: Color.fromARGB(255, 158, 86, 246))
            )
          ],
        )
      );
  }

  @override

  Size get preferredSize => const Size.fromHeight(60.0);

}