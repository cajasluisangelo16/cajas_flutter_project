import 'package:cajas_flutter/pages/item_info.dart';
import 'package:flutter/material.dart';

import '../components/item_container.dart';

class Homepage extends StatelessWidget  {

  const Homepage({
     Key? key,
    
  }) : super (key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 20),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:  [
                    GestureDetector(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> const Items()));
                      },
                      child: const ItemContainer(imagePath: '')),
                    const SizedBox(width: 50),
                    const ItemContainer(imagePath: ''),

                  ],
                ),
                const SizedBox(height: 20),

                 Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    ItemContainer(imagePath: ''),
                    SizedBox(width: 50),
                    ItemContainer(imagePath: ''),

                  ],
                ),
                const SizedBox(height: 20),

                 Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    ItemContainer(imagePath: ''),
                    SizedBox(width: 50),
                    ItemContainer(imagePath: ''),

                  ],
                ),
                 const SizedBox(height: 20),
               

                 Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    ItemContainer(imagePath: ''),
                    SizedBox(width: 50),
                    ItemContainer(imagePath: ''),

                  ],
                ),
                 const SizedBox(height: 20),

                 Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    ItemContainer(imagePath: ''),
                    SizedBox(width: 50),
                    ItemContainer(imagePath: ''),

                  ],
                ),
                 const SizedBox(height: 20),
              ],
              
            ),
            
          )
        ),
       
      ), 
    

    );
  }

  

}