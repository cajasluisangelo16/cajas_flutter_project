import 'package:flutter/material.dart';
import '../components/cart_item.dart';
import 'checkout.dart';

class Cart extends StatelessWidget  {

 void _loginIn(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Checkout()),
    );
  }

  const Cart({
     Key? key,
    
  }) : super (key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Center(
            child:SingleChildScrollView(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children:  [
               const SizedBox(height: 20),
               const CartItem(imagePath: ''),
               const SizedBox(height: 20),
               const CartItem(imagePath: ''),
               const SizedBox(height: 10),
               Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0),
                    child: Row(
                      children: const [
                        Expanded(
                          child: Divider(
                            thickness: 1,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text('Total:',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                      SizedBox(width: 160),
                      Text('Php 23,998.00',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16, fontStyle: FontStyle.italic,color: Colors.purple),)
                  ],),
                  const SizedBox(height: 160,),
                   ElevatedButton(
                  onPressed: () {
                    _loginIn(context);
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(158, 86, 246, 1),
                    shape: const StadiumBorder(),
                    padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 120),
                  ),
                  child: const Text(
                    "CHECKOUT",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                
            
               
              ],
              
              ),
            )
            
            
          )
        
       
      ), 
    

    );
    
  }
  

}