import 'package:cajas_flutter/pages/login_page.dart';
import 'package:flutter/material.dart';

import '../pages/pay_items.dart';
import '../pages/receive_items.dart';
import '../pages/review_items.dart';
import '../pages/ship_items.dart';

class Profile extends StatelessWidget  {

void _signOut(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>  Login()),
    );
  }
void _toPay(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>  const Pay()),
    );
  }
void _toShip(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Ship()),
    );
  }
  void _toReceive(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Receive()),
    );
  }
  void _toReview(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Review()),
    );
  }
  
  const Profile({
     Key? key,
    
  }) : super (key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 90),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text('Hello',
                    style: TextStyle(fontSize: 32,fontWeight: FontWeight.bold),
                    ),
                    SizedBox(width: 10),
                    Text('Luis', style: TextStyle(
                      fontSize: 32, fontWeight: FontWeight.bold,
                      color: Color.fromARGB(255, 158, 86, 246,)),),
                      Text('!',
                    style: TextStyle(fontSize: 32,fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                const SizedBox(height: 80),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Divider(
                          thickness: 0.8,
                          color: Colors.grey[400],
                        ),
                      ),
                      
                    ],
                  ),
                ),

                const SizedBox(height: 30),

                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    SizedBox(width: 50),
                    Text('Your Items',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)
                  ],
                ),
                const SizedBox(height: 40),
                Padding(
                  
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [

                    IconButton(
                    icon: Image.asset('lib/images/pay_icon.png'),
                    onPressed: () {
                      _toPay(context);
                    },
                    ),
                    IconButton(
                    icon: Image.asset('lib/images/ship_icon.png'),
                    
                    onPressed: () {
                      _toShip(context);
                    },
                    ),
                    IconButton(
                    icon: Image.asset('lib/images/receive_icon.png'),
                    onPressed: () {
                    _toReceive(context);
                    },
                    ),
                    IconButton(
                    icon: Image.asset('lib/images/review_icon.png'),
                    onPressed: () {
                      _toReview(context);
                    },
                    )
                  ],
                ),
                 
                  ),
            
                const SizedBox(height: 20),
                Padding(
                  
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      
                      Text('To Pay',style: TextStyle(fontWeight: FontWeight.bold),),
                      
                    
                      Text('To Ship',style: TextStyle(fontWeight: FontWeight.bold)),
                    
                      Text('To Receive',style: TextStyle(fontWeight: FontWeight.bold)),
                    
                   
                      Text('To Review',style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),

                const SizedBox(height:180),

                 ElevatedButton(
                onPressed: () {
                  _signOut(context);
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromRGBO(158, 86, 246, 1),
                  shape: const StadiumBorder(),
                  padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 120),
                ),
                child: const Text(
                  "Sign Out",
                  style: TextStyle(fontSize: 20),
                ),
              ),
               
                
            
              ],
              
            ),
            
          )
        
       
      ), 
    

    );
    
  }

  

}