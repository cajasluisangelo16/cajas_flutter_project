import 'package:cajas_flutter/pages/ship_items.dart';
import 'package:flutter/material.dart';
import '../components/cart_item.dart';
import 'app_bar.dart';

class Checkout extends StatelessWidget  {


  const Checkout({
     Key? key,
    
  }) : super (key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(),
      body: SafeArea(
          child: Center(
            child:SingleChildScrollView(
              child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children:  [
               const SizedBox(height: 10),
               const CartItem(imagePath: ''),
               const SizedBox(height: 10),
               const CartItem(imagePath: ''),
               const SizedBox(height: 10),
               Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0),
                    child: Row(
                      children: const [
                        Expanded(
                          child: Divider(
                            thickness: 1,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40),
                  child: Row(mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Text('Full Address', 
                    style: TextStyle(fontWeight: FontWeight.bold),)
                  ]),),
                  const SizedBox(height: 5),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                   child: TextField(
                    
                    decoration: InputDecoration(
                    border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(40),
                  ),
                  filled: true,
                  ),
                  ),
                  ),
                  const SizedBox(height: 10),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40),
                  child: Row(mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Text('Payment Option', 
                    style: TextStyle(fontWeight: FontWeight.bold),)
                  ]),),
                  const SizedBox(height: 5),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                   child: TextField(
                    
                    decoration: InputDecoration(
                    border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(40),
                  ),
                  filled: true,
                  hintText: '\tCash on Delivery',
                  
                  hintStyle: TextStyle(color: Colors.grey[500])),
                  ),
                  ),
                  const SizedBox(height: 10),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40),
                  child: Row(mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Text('Contact', 
                    style: TextStyle(fontWeight: FontWeight.bold),)
                  ]),),
                  const SizedBox(height: 5),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                   child: TextField(
                    decoration: InputDecoration(
                    border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(40),
                  ),
                  filled: true,
                  ),
                  ),
                  ),
                  const SizedBox(height: 10),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40),
                  child: Row(mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Text('Promo Code', 
                    style: TextStyle(fontWeight: FontWeight.bold),)
                  ]),),
                  const SizedBox(height: 5),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30.0),
                   child: TextField(
                   
                    decoration: InputDecoration(
                    border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  filled: true,
                 ),
                  ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Text('Total:',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                      SizedBox(width: 160),
                      Text('Php 23,998.00',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16, fontStyle: FontStyle.italic,color: Colors.purple),)
                  ],),
                  const SizedBox(height: 20,),
                   ElevatedButton(
                  onPressed: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> const Ship()));
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromRGBO(158, 86, 246, 1),
                    shape: const StadiumBorder(),
                    padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 120),
                  ),
                  child: const Text(
                    "PLACE ORDER",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                const SizedBox(height: 30)
                
            
               
              ],
              
              ),
            )
            
            
          )
        
       
      ), 
    

    );
  }

  

}