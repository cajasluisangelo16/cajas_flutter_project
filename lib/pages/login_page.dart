import 'package:cajas_flutter/pages/signup_page.dart';
import 'package:flutter/material.dart';
import 'package:cajas_flutter/components/my_textfield.dart';
import 'package:cajas_flutter/components/square_tile.dart';
import 'package:cajas_flutter/pages/main_page.dart';

class Login extends StatelessWidget{
  Login({super.key});



   // text editing controllers
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  // sign user in method
  void _loginIn(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => const BottomNavigationBarExample()),
    );
  }
  void _loginIn2(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Signup()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center ( 
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 10),
          
                // logo
                Image.asset('lib/images/branding.png',
                height: 135,
                 fit: BoxFit.fitWidth,
                ),
          
                const SizedBox(height: 30),
          
                // welcome back text
                const Text(
                  'Welcome back !',
                  style: TextStyle(
                    color: Color.fromARGB(255, 0, 0, 0),
                    fontSize: 26,
                  ),
                ),
                const SizedBox(height: 10),
                 Text(
                  'Login using Username and Password',
                  style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 16,
                    
                  ),
                ),
                const SizedBox(height: 30),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const[
                    Text(
                      'Username',
                      style: TextStyle(color: Color.fromARGB(255, 0, 0, 0),
                      fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
                const SizedBox(height: 10),
          
                // username textfield
                MyTextField(
                  controller: usernameController,
                  hintText: ' ',
                  obscureText: false,
                ),
          
                const SizedBox(height: 10),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const[
                    Text(
                      'Password',
                      style: TextStyle(color: Color.fromARGB(255, 0, 0, 0),
                      fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
                const SizedBox(height: 10),
                // password textfield
                MyTextField(
                  controller: passwordController,
                  hintText: ' ',
                  obscureText: true,
                ),
          
                const SizedBox(height: 15),
          
              ElevatedButton(
                onPressed: () {
                  _loginIn(context);
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromRGBO(158, 86, 246, 1),
                  shape: const StadiumBorder(),
                  padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 140),
                ),
                child: const Text(
                  "Sign In",
                  style: TextStyle(fontSize: 20),
                ),
              ),
               
               const SizedBox(height: 25),
          
                // or continue with
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Divider(
                          thickness: 0.5,
                          color: Colors.grey[400],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          'Or Sign in with',
                          style: TextStyle(color: Colors.grey[700]),
                        ),
                      ),
                      Expanded(
                        child: Divider(
                          thickness: 0.5,
                          color: Colors.grey[400],
                        ),
                      ),
                    ],
                  ),
                ),
          
                const SizedBox(height: 30),
          
                // google + apple sign in buttons
                Row( 
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    // google button
                    SquareTile(imagePath: 'lib/images/googlelogo.png'),
          
                    SizedBox(width: 25),
          
                    // apple button
                    SquareTile(imagePath: 'lib/images/applelogo.png'),

                    SizedBox(width: 25),

                    SquareTile(imagePath: 'lib/images/fblogo.png')
                  ],
                ),
          
                const SizedBox(height: 50),
          
                // not a member? register now
        
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Create a new account?',
                      style: TextStyle(color: Colors.grey[700]),
                    ),
                    const SizedBox(width: 4),
                 TextButton(
                 onPressed: () {
                  _loginIn2(context);
                 },
                child: const Text('Sign Up')
                ),
              
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
