import 'package:cajas_flutter/components/my_textfield.dart';
import 'package:flutter/material.dart';

class Signup extends StatelessWidget{
  Signup({super.key});
  
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final emailController = TextEditingController();
  
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 20),

                //welcome
                const Text(
                  'Welcome!',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 35,
                    fontWeight: FontWeight.bold
                  ),
                ),
                const SizedBox(height: 10,),
                //create new acc text
                const Text(
                  'Create new account',
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 16
                  ),
                ),
                const SizedBox(height: 30),
                //name textfield
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    SizedBox(height:25),
                    Text(
                      'Name',
                      style: TextStyle(color: Colors.black,
                      fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
                MyTextField(
                controller: usernameController, 
                hintText: ' ', 
                obscureText: false),
                const SizedBox(height: 10),
                //email textfield
                Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const[
                    SizedBox(height:25),
                    Text(
                      'Email',
                      style: TextStyle(color: Colors.black,
                      fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
                MyTextField(
                controller: emailController, 
                hintText: ' ', 
                obscureText: false),
                const SizedBox(height: 10),
                //password textfield
                Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    SizedBox(height:25),
                    Text(
                      'Password',
                      style: TextStyle(color: Colors.black,
                      fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
                MyTextField(
                controller: passwordController, 
                hintText: ' ', 
                obscureText: true),
                const SizedBox(height: 15),
                //signup button
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: const Color.fromARGB(255, 158, 86, 246),
                  shape: const StadiumBorder(),
                  padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 140),
                ),
                child: const Text(
                  "Sign In",
                  style: TextStyle(fontSize: 20),
                ),
              ),
                const SizedBox(height: 50),
                //Already have an account text 
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Already have an account?',
                      style: TextStyle(color: Colors.grey[700]),
                    ),
                    const SizedBox(width: 4),
                     TextButton(
                 onPressed: () {
                  Navigator.pop(context);
                 },
                child: const Text('Sign In'),
                ),
                  ],
                )

                
              ],
            ),
          ),
        )
        )
    );
  }


}