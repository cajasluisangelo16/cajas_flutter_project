import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cajas_flutter/pages/main_page.dart';
import 'package:flutter/material.dart';

class Items extends StatelessWidget  {

  const Items({
     Key? key,
    
  }) : super (key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 20,),
                Padding(
                  padding:  const EdgeInsets.symmetric(horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Ink (
                      decoration: const ShapeDecoration(shape: CircleBorder(), color: Color.fromRGBO(158, 86, 246, 1)),
                      child: IconButton(onPressed: (){
                        Navigator.pop(context);
                      }, icon: const BackButtonIcon())),
                      Ink (
                      decoration: const ShapeDecoration(shape: CircleBorder(), color: Color.fromRGBO(158, 86, 246, 1)),
                      child: IconButton(onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> const BottomNavigationBarExample()));
                      }, icon: const Icon(Icons.share))),
                    ],
                  ),
                ),
                Column(mainAxisAlignment: MainAxisAlignment.center,children: [
                  Image.asset('lib/images/product.png', fit: BoxFit.fitWidth, width: double.infinity)
                  ]
                  ),
                  Row(
                    children:[
                      Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 50),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                        Text('Adidas Shoe',
                        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                        SizedBox(height: 10),
                        Text('Php 11,999.00',
                        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, 
                        fontStyle: FontStyle.italic, color: Color.fromRGBO(158, 86, 246, 1)),)
                      ],)
                      ),
                    ] 
                  ),
                  const SizedBox(height: 80),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: AnimatedButton(
                        color: const Color.fromRGBO(158, 86, 246, 1),
                        text: 'Add to cart',
                        pressEvent: (){AwesomeDialog(
                          context: context,
                          dialogType: DialogType.success,
                          animType: AnimType.topSlide,
                          showCloseIcon: true,
                          title: 'Added to your cart',
                          btnCancelOnPress: (){},
                          btnOkOnPress: (){},
                           ).show();} 
                      
                      ),
                    ),


                  const SizedBox(height: 20),
                   Row(
                    children:[
                      Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 50),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                        Text('More Details',
                        style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),),
                        SizedBox(height: 5),
                        Text('Gear up with the latest collections fromadidas \nOriginals, Running, Football, Training. With over \n20,000+ products, you will never run out of choice. \nGrab your favorites now. Secure Payments. \n100% Original Products. Gear up with adidas.',
                        style: TextStyle(fontSize: 14, 
                        fontStyle: FontStyle.italic, color: Color.fromRGBO(116, 116, 116, 1)),)
                      ],)
                      ),
                    ] 
                  ),
              ],
              
            ),
             
          )
        ),
       
      ), 
    

    );
  }

  

}