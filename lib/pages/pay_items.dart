import 'package:cajas_flutter/components/cart_item.dart';
import 'package:cajas_flutter/pages/receive_items.dart';
import 'package:cajas_flutter/pages/review_items.dart';
import 'package:cajas_flutter/pages/ship_items.dart';
import 'package:flutter/material.dart';

class Pay extends StatelessWidget  {

  const Pay({
     Key? key,
    
  }) : super (key: key);

  void _toShip(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => const Ship()),
    );
  }
  void _toReceive(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => const Receive()),
    );
  }
  void _toReview(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => const Review()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 30,),
                Padding(
                  padding:  const EdgeInsets.symmetric(horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Ink (
                      decoration: const ShapeDecoration(shape: CircleBorder(), color: Color.fromRGBO(158, 86, 246, 1)),
                      child: IconButton(onPressed: (){
                        Navigator.pop(context);
                      }, icon: const BackButtonIcon())),
                      const SizedBox(width: 15),
                      const Text('My Orders', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),)
                    ],
                  ),
                ),
                const SizedBox(height: 15),
                Padding(
                  padding:  const EdgeInsets.symmetric(horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children:  [

                      const Text('To Pay',
                      style: TextStyle(fontWeight: FontWeight.bold,color:  Color.fromARGB(255, 158, 86, 246,))),
                      
                      TextButton(
                        onPressed: () {
                          _toShip(context);
                        },
                        child: const Text('To Ship',
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black))),
                      
                      TextButton(
                        onPressed: () {
                          _toReceive(context);
                        },
                        child: const Text('To Receive',
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black))),
                       
                       TextButton(
                        onPressed: () {
                          _toReview(context);
                        },
                        child: const Text('To Review',
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black))),
                    ],
                  ),
                ),
                const SizedBox(height: 30),
                const CartItem(imagePath: ''),
                const SizedBox(height: 10),
                const CartItem(imagePath: ''),
                const SizedBox(height: 10),
                const CartItem(imagePath: ''),
              ],
              
            ),
             
          )
        ),
       
      ), 
    

    );
  }

  

}